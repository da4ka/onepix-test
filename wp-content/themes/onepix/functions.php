<?php

include_files_from_directory(__DIR__ ."/inc");
include_files_from_directory(__DIR__ ."/inc/theme-settings");
include_files_from_directory(__DIR__ ."/inc/post-types/buildings");
include_files_from_directory(__DIR__ ."/inc/post-types/metro");


include_files_from_directory(__DIR__ ."/inc/utils"); // additional funtions using in theme files

function include_files_from_directory($dir){
  if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
      while ($filename = readdir($dh)) {
        if ( $filename[0] != '-' ){
          $filename = $dir."/".$filename;
          $file_parts = pathinfo($filename);
          if ( array_key_exists('filename', $file_parts) && $file_parts['filename'] && $file_parts['filename'][0] != "_" ){
            if (array_key_exists('extension', $file_parts) && $file_parts['extension'] == "php"){
              include_once($filename);
            }
          }
        }
      }
      closedir($dh);
    }
  }
}













