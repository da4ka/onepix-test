<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Onepix
 * @since 1.0.0
 */

get_header();
wp_enqueue_script('loadmore');
wp_enqueue_script( 'maps' );

?>

<?php // TOGO : breadcrumbs ?>
<div class="page-top">
  <nav class="page-breadcrumb" itemprop="breadcrumb">
    <a href="/">Главная</a>
    <span class="breadcrumb-separator"> > </span>
     <a href="buildings.html">Новостройки</a><span class="breadcrumb-separator"> > </span> 
    Расцветай на Маркса
  </nav>
</div>

<div class="page-section">

  <div class="page-content">

    <article class="post">

      <div class="post-header">

        
        <?php the_title('<h1 class="page-title-h1">', '</h1>'); ?>
        <?php if ( get_field('company') ) : ?>
          <span><?php the_field('company') ?></span>
        <?php endif; ?>

        <div class="post-header__details">
          
          <?php if ( get_field('adress') ) : ?>
            <div class="address"><?php the_field('adress') ?></div>
          <?php endif; ?>

          <?php 
            if ( $stations = get_field('metro') ){
              foreach ($stations as $station) {
                echo '<div class="metro">';
                echo '<span class="icon-metro icon-metro--'.get_field('branch', $station['station']).'"></span>';
                echo '<span class="page-text">'.get_the_title($station['station']).' <span> '.custom_time_format($station['time']).'</span></span>';
                echo '<span class="icon-'.$station['transport'].'"></span>';
                echo '</div>';
              }
            }
          ?>   

        </div>

      </div>

      <div class="post-image">
        <?php the_post_thumbnail('medium'); ?>
        <?php get_template_part('template-parts/buildings/single', 'tags'); ?>

        <?php /* TODO : save favorites to user cookies or local storage and create page "favorites" */ ?>
        <a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное" role="button">
        <span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
        </a>
      </div>

      <?php get_template_part('template-parts/buildings/single', 'opts'); ?>

      <?php if ( get_the_content() ) : ?>
        <h2 class="page-title-h1">Краткое описание</h2>
        <div class="post-text">
          <?php the_content(); ?>
        </div>
      <?php endif; ?>

      <?php if ( get_field('lat') && get_field('lng') ) : ?>
        <h2 class="page-title-h1">Карта</h2>
        <script>
          var building_lat = <?php the_field('lat'); ?>;
          var building_lng = <?php the_field('lng'); ?>;
        </script>
        <div class="post-map" id="post-map" style="width: 100%; height: 300px;"></div>
      <?php endif; ?>

    </article>
  </div>
  <div class="page-filter"></div>
</div> 

<?php get_footer();