<?php
/**
 * The template for displaying error 404 page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#404-not-found
 *
 * @package WordPress
 * @subpackage Onepix
 */

get_header();
?>

<div class="homepage__content">
  <div class="text-center" style="text-align: center">
    <h1>404</h1><br><br>
    <p>Oops, page not found =( </p>
    <br><br>
    <a href="<?php echo get_post_type_archive_link('buildings'); ?>" class="button button--pink">В каталог</a>

  </div>
</div>


<?php get_footer();
