"use strict";

jQuery(document).ready(function($){

  $('.wp_loadmore').click(function(){


    var button = $(this),
      data = {
        'action': 'loadmore',
        'query': loadmore_params.posts, 
        'page' : loadmore_params.current_page,
        'template': loadmore_params.template,
        'loading_text' : loadmore_params.loading_text,
        'urlbase' : loadmore_params.urlbase,
        'get' : loadmore_params.get
      };


    var text = button.text();
    var pagination = button.parent().parent().find('.pagination');
 
    $.ajax({ 
      url : loadmore_params.ajaxurl, 
      data : data,
      type : 'POST',
      beforeSend : function ( xhr ) {
        button.data('text-text', button.find('.anim-btn').html());
        button.find('.anim-btn').html(data.loading_text); 
        button.addClass('loading');
      },
      success : function( data ){
        if( data ) { 

          button.find('.anim-btn').html( button.data('text-text') ); 
          button.removeClass('loading');

          loadmore_params.current_page++;
 
          if ( loadmore_params.current_page == loadmore_params.max_page ) 
            button.parent().remove(); 


          if ($(loadmore_params.result_block).hasClass('grid')){
            $(loadmore_params.result_block).isotope('insert', $(data));

              var filters = [];
              if ($('.project_select_items .active').length){
                if ($('.project_select_items .active').attr('data-filter')!='*')
                  filters.push($('.project_select_items .active').attr('data-filter') );
              }
              if ($('.year_select_items .active').length){
                if ($('.year_select_items .active').attr('data-filter')!='*')
                  filters.push($('.year_select_items .active').attr('data-filter') );
                  
              }


            if (filters.length==0){
              $(loadmore_params.result_block).isotope({filter: '*'});
            }else{
              var filters_srt = filters.join('');
              $(loadmore_params.result_block).isotope({filter: filters_srt});
              var n = 0;
              $('.grid .vp-grid__item').each(function(){
                if ($(this).css('display') == 'block'){
                  n++;
                }
              })

              if (n<1 && $('.wp_loadmore').length ) $('.wp_loadmore').trigger("click");
            }
            
          }else{
            $(loadmore_params.result_block).append(data);
          }

          if (loadmore_params.urlbase){
            var url = loadmore_params.urlbase.replace('page/'+(loadmore_params.current_page-1)+ '/', '');

            if ( loadmore_params.get != '' ) loadmore_params.get = '?'+loadmore_params.get;

            // window.history.pushState({}, document.title, url+loadmore_params.current_page+loadmore_params.get);
          }
 
        } else {
          button.remove(); 
        }


        if ( pagination.length ){
          var pagTemp = false;
          var url = window.location.href.split('/page')[0];


          if ( $(loadmore_params.result_block).find('.pagination-temp').length ){
            pagTemp = $(loadmore_params.result_block).find('.pagination-temp').html();
            $(loadmore_params.result_block).find('.pagination-temp').remove();
            pagination.html($(pagTemp).find('.pagination').html())
            pagination.find('a').each(function(){
              let link = $(this).attr('href').split('/page')[1];
              if ( typeof(link) == 'undefined' ){
                link = '';
              }else{
                link = '/page' + link;
              }
              $(this).attr('href', url+link)
            })

          }
        } 
      }
    });

    return false;

  })
})