<li class="page-loop__item wow animate__animated animate__fadeInUp" data-wow-duration="0.8s">
  <?php /* TODO : save favorites to user cookies or local storage and create page "favorites" */ ?>
  <a href="#" class="favorites-link favorites-link__add" title="Добавить в Избранное" role="button">
    <span class="icon-heart"><span class="path1"></span><span class="path2"></span></span>
  </a>

  <a href="<?php the_permalink(); ?>" class="page-loop__item-link">

    <div class="page-loop__item-image">
      <?php 
        // TODO : add default image
        the_post_thumbnail(); 
      ?>
      
      <?php get_template_part('template-parts/buildings/single', 'tags'); ?>
    </div>

    <div class="page-loop__item-info">
      <h3 class="page-title-h3"><?php the_title(); ?></h3>
      
      <?php if ( get_field('term') ) : ?>
      <p class="page-text">Срок сдачи <?php the_field('term'); ?></p>
      <?php endif; ?>
             
      <?php 
        if ( $stations = get_field('metro') ){
          $station = $stations[0];
          echo '<div class="page-text to-metro">';
          echo '<span class="icon-metro icon-metro--'.get_field('branch', $station['station']).'"></span>';
          echo '<span class="page-text">'.get_the_title($station['station']).' <span> '.custom_time_format($station['time']).'</span></span>';
          echo '<span class="icon-'.$station['transport'].'"></span>';
          echo '</div>';
        }
      ?>      

      <?php if ( get_field('adress') ) : ?>
      <span class="page-text text-desc"> <?php the_field('adress'); ?></span>
      <?php endif; ?>
    </div>
  </a>
</li>