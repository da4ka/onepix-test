<?php if ( $tags = get_the_terms(get_the_ID(), 'buildings_tags') ) : ?>
  <div class="page-loop__item-badges">
  <?php
    foreach ($tags as $tag) {
      echo '<span class="badge">'.$tag->name.'</span>';
    }
  ?>
  </div>
  <?php endif; ?>