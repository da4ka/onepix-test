<?php // TOGO : output of this fields ?>
<h2 class="page-title-h1">Характеристики ЖК</h2>

<ul class="post-specs">
<li>
<span class="icon-building"></span>
<div class="post-specs__info">
  <span>Класс жилья</span>
  <p>Комфорт</p>
</div>
</li>
<li>
<span class="icon-brick"></span>
<div class="post-specs__info">
  <span>Конструктив</span>
  <p>Монолит-кирпич</p>
</div>
</li>
<li>
<span class="icon-paint"></span>
<div class="post-specs__info">
  <span>Отделка</span>
  <p>
    Чистовая
    <span class="tip tip-info" data-toggle="popover" data-placement="top"
      data-content="And here's some amazing content. It's very engaging. Right?">
      <span class="icon-prompt"></span>
    </span>
  </p>
</div>
</li>
<li>
<span class="icon-calendar"></span>
<div class="post-specs__info">
  <span>Срок сдачи</span>
  <p>4 кв. 2020</p>
</div>
</li>
<li>
<span class="icon-ruller"></span>
<div class="post-specs__info">
  <span>Высота потолков</span>
  <p>2,7 м</p>
</div>
</li>
<li>
<span class="icon-parking"></span>
<div class="post-specs__info">
  <span>Подземный паркинг</span>
  <p>Присутствует</p>
</div>
</li>
<li>
<span class="icon-stair"></span>
<div class="post-specs__info">
  <span>Этажность</span>
  <p>10-17</p>
</div>
</li>
<li>
<span class="icon-wallet"></span>
<div class="post-specs__info">
  <span>Ценовая группа</span>
  <p>Выше среднего</p>
</div>
</li>
<li>
<span class="icon-rating"></span>
<div class="post-specs__info">
  <span>Рейтинг</span>
  <p>8.8</p>
</div>
</li>
</ul>