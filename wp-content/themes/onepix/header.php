<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until content
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Onepix
 */


?>
<!DOCTYPE html> 
<html <?php language_attributes(); ?>>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detection" content="address=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title(); ?></title>

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
   
  <main class="main">
    <div class="container">