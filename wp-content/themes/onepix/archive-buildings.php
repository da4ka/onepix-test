<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Onepix
 * @since 1.0.0
 */

get_header();
wp_enqueue_script('loadmore');

?>
<div class="page-top">

  <?php // TOGO : breadcrumbs ?>
  <nav class="page-breadcrumb" itemprop="breadcrumb">
    <a href="/">Главная</a>
    <span class="breadcrumb-separator"> > </span>    
    Новостройки
  </nav>

  <div class="page-top__switchers">

    <div class="container">
      <div class="row">

        <div class="page-top__switchers-inner">

          <a href="#" class="page-top__filter">
            <span class="icon-filter"></span>
            Фильтры
          </a>

          <a href="#" data-tab-name="loop" class="page-top__switcher tab-nav active">
            <span class="icon-grid"></span>
          </a>

          <a href="#" data-tab-name="map" class="page-top__switcher tab-nav">
            <span class="icon-marker"></span>
          </a>

        </div>

      </div>
    </div>

  </div>

</div>

<div class="page-section">

  <div class="page-content">

    <h1 class="visuallyhidden"><?php the_archive_title(); ?></h1>

    <div class="page-loop__wrapper loop tab-content tab-content__active">

      <?php if ( have_posts() ) :  ?>

        <ul class="page-loop with-filter">

        <?php 
          while ( have_posts() ) :
            the_post();
            get_template_part('template-parts/buildings/single', 'content');
          endwhile;
        ?>    

        </ul>

 
        <?php 
          $url = explode('?', $_SERVER['REQUEST_URI']);
          $url_base = $url[0];
          $get = false;
          if ( isset($url[1]) ) $get = $url[1];
        ?>
        <?php loadmore('.page-loop', 'template-parts/buildings/single-content', ' Показать еще', 'Загрузка...', false, $get); ?>

      <?php else : ?>
        Не найдено
      <?php endif; ?>

    </div>

    <div class="page-map tab-content map">

      <!-- it is bad idea for H1 -->
      <h1>Тут будет карта</h1>

    </div>

  </div>

  <?php get_sidebar(); ?>
</div>
 

<?php get_footer();
