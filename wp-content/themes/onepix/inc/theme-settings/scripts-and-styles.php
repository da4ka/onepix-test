<?php

// register scripts ans styles

function custom_scripts_styles() {    
  
    global $wp_styles;

    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/fonts/icomoon/icon-font.css', array(), '', 'screen');
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/libs/animate/animate.min.css', array(), '', 'screen');

    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/style.min.css', array(), '', 'screen');

    wp_enqueue_style( 'site-style', get_stylesheet_uri() ); 

    if (!is_user_logged_in()){
        wp_deregister_style( 'dashicons' ); 
    }


    // scripts

    wp_deregister_script('jquery');
    wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', array(), '', true );
    wp_enqueue_script( 'popper', get_template_directory_uri() . '/libs/bootstrap/js/popper.min.js', array(), '', true );

    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', array('popper'), '', true );
    wp_enqueue_script( 'ofi', get_template_directory_uri() . '/libs/ofi/ofi.min.js', array(), '', true );
    wp_enqueue_script( 'wowjs', get_template_directory_uri() . '/libs/wowjs/wow.min.js', array(), '', true );


    wp_register_script( 'maps', 'https://api-maps.yandex.ru/2.1/?apikey=f7f5866c-fcab-4da8-94d7-cdbdb39c7d22&lang=ru_RU', array(), '', true );

    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/scripts.js', array('jquery', 'bootstrap', 'ofi', 'wowjs' ), '', true );
    
    wp_register_script( 'loadmore', get_template_directory_uri() . '/js/loadmore.js', array('jquery'), '', true );


    

}

add_action( 'wp_enqueue_scripts', 'custom_scripts_styles', 100  );



add_filter('style_loader_tag', 'js_remove_type_attr', 1000, 2);
add_filter('script_loader_tag', 'js_remove_type_attr', 1000, 2);


// remove type for tag script (w3c)
function js_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

add_filter('autoptimize_filter_imgopt_lazyload_cssoutput','no_noscript');
function no_noscript() {
    return '<style>.lazyload,.lazyloading{opacity:0;}.lazyloaded{opacity:1;transition:opacity 300ms;}</style>';
}