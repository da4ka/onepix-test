<?php

	add_filter( 'wpcf7_form_elements', 'imp_wpcf7_form_elements' );
	function imp_wpcf7_form_elements( $content ) {
	    $str_pos = strpos( $content, 'type="file"' );
	    if ( $str_pos !== false ) {
	        $content = substr_replace( $content, ' data-placeholder="'.__('Select file', 'appvesto').'" ', $str_pos, 0 );
	    }
	    return $content;
	}