<?php

  // TODO : data validation 

  add_action( 'pre_get_posts', 'buildings_filter' );
  function buildings_filter( $query ) {


    if ( $_GET && $query->query_vars['post_type'] == 'buildings' ){

      $tax = $query->query_vars['tax_query'];
      $meta = $query->query_vars['meta_query'];

      $updated_tax = $tax;
      $updated_meta = $meta;

      // 1. distance filter

      if ( !isset($_GET['distance_any']) ){
        
        $distance = array('relation' => 'OR');

        if (isset($_GET['distance_less10'])){
          $distance[] = array(
            'key'     => 'distance',
            'value'   => 10,
            'compare' => '<=',
            'type'    => 'NUMERIC',
          );
        }

        if (isset($_GET['distance_form_10_to_20'])){
          $distance[] = array(
            'key'     => 'distance',
            'value'   => array(10, 20),
            'compare' => 'BETWEEN',
            'type'    => 'NUMERIC',
          );
        }

        if (isset($_GET['distance_form_20_to_40'])){
          $distance[] = array(
            'key'     => 'distance',
            'value'   => array(20, 40),
            'compare' => 'BETWEEN',
            'type'    => 'NUMERIC',
          );
        }

        if (isset($_GET['distance_more_40'])){
          $distance[] = array(
            'key'     => 'distance',
            'value'   => 40,
            'compare' => '>=',
            'type'    => 'NUMERIC',
          );
        }

        $updated_meta[] = $distance;

      }

      // 2. Term ( TOGO: term filter )

      // 3. Class

      $classes = ['economical', 'comfort', 'business', 'elite'];
      $classes_active = array();
      foreach ($classes as $class) {
        
        if ( isset($_GET[$class]) && $_GET[$class] = 'on' ){
          $classes_active[] = $class; 
        }
      }

      if ($classes_active){          
        $updated_meta[] = array(
          'key'     => 'class',
          'value'   => $classes_active,
          'compare' => 'IN',
        );
      }

      // 4. Options & additionasl options

      $options = ['opt1', 'opt2', 'opt3', 'opt4', 'opt5', 'opt6', 'opt7', 'add1', 'add2', 'add3', 'add4', 'add5'];
      
      foreach ($options as $opt) {
        
        if ( isset($_GET[$opt]) && $_GET[$opt] = 'on' ){
          $updated_meta[] = array(
            'key'     => $opt,
            'value'   => '1',
            'compare' => '==',
          );          
        }
      }


      if ( isset($_GET['service']) && $_GET['service'] = 'on' ){
        $updated_tax[] = array(
          'taxonomy'     => 'buildings_tags',
          'field'   => 'slug',
          'terms' => array('free-services'),
          'operator' => 'IN'
        );        
      }

      //var_dump( $updated_tax );
      //var_dump( $updated_meta );


      if ($updated_meta){
        set_query_var('meta_query', $updated_meta);
      }
      if ($updated_tax){
        set_query_var('tax_query', $updated_tax);
      }


      //var_dump( $query );

    }




  }

