<?php


function samples_register_post_type() {
  $args = array(    
    'labels' => array(
      'name' => 'Новостройки',
      'menu_name' => 'Новостройки',
      'singular_name' => 'Новостройка'
    ),
    'rewrite' => array(
      'slug' => 'buildings',
    ),
    'public' => true,
    'show_ui' => true,
    'publicly_queryable' => true,
    'supports' => array( 'title', 'thumbnail', 'editor' ),
    'has_archive' => true,
    'menu_icon' => 'dashicons-admin-home',
  );
  register_post_type('buildings', $args);


  register_taxonomy(
      'buildings_tags',
      array('buildings'),
      array(
        'labels'        => array(
          'name'              => __( 'Теги' ),
          'singular_name'     => __( 'Тег' ),
          'menu_name'         => __( 'Теги' ),
        ),
        'show_ui' => true,
        'query_var' => true,
        'public' => true,
        'rewrite' => array( 'slug' => 'buildings-tag' ),
        'hierarchical' => false,
      )
  );

}

add_action( 'init', 'samples_register_post_type', 10 );