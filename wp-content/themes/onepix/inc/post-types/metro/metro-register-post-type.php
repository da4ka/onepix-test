<?php


function metro_register_post_type() {
  $args = array(    
    'labels' => array(
      'name' => 'Метро',
      'menu_name' => 'Метро',
      'singular_name' => 'Станция'
    ),
    'rewrite' => array(
      'slug' => 'metro',
    ),
    'public' => false,
    'show_ui' => true,
    'publicly_queryable' => false,
    'supports' => array( 'title' ),
    'has_archive' => false,
  );
  register_post_type('metro', $args);


}

add_action( 'init', 'metro_register_post_type', 10 );