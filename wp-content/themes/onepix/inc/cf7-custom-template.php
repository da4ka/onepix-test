<?php

	function formatform_function( $atts = array() ) {

	  
	    // set up default parameters
	    extract(shortcode_atts(array(
	     'formid' => ''
	    ), $atts));


	    if ($formid){

	    	$form = '<div class="mainform">';
	    	$form .= '<div class="mainform__header">'.get_the_title($formid).'</div>';
	    	$form .= '<div class="mainform__content">'.do_shortcode('[contact-form-7 id="'.$formid.'" "]').'</div>';
	    	$form .= '</div>';
	    	return $form;

	    }else{
	    	return false;
	    }
	    
	}

	add_shortcode('formatform', 'formatform_function');