<?php 
  function loadmore($selector, $template, $title = 'Load more', $loading_text = 'Loading...', $urlbase = false, $get = false){
    global $wp_query; 
 
    if (  $wp_query->max_num_pages > 1 && get_query_var('paged') < $wp_query->max_num_pages ){
      echo '<div class="show-more">
      <button href="#" class="show-more__button load-more wp_loadmore" data-text="'.$title.'">
        <span class="show-more__button-icon"></span>
        <span class="anim-btn">'.$title.'</span>
      </button>
    </div>';
    
      wp_register_script( 'loadmore', get_stylesheet_directory_uri() . '/js/loadmore.js' );
   
      wp_localize_script( 'loadmore', 'loadmore_params', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'posts' => json_encode( $wp_query->query_vars ),
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages,
        'result_block' => $selector,
        'template' => $template,
        'loading_text' => $loading_text,
        'urlbase' => $urlbase,
        'get' => $get,
      ) );

      wp_enqueue_script( 'loadmore');

    }
  }


  function loadmore_ajax_handler(){
 
    // prepare our arguments for the query
    $args = json_decode( stripslashes( $_POST['query'] ), true );
    $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';
    $args['template'] = $_POST['template'];

    // it is always better to use WP_Query but not here
    query_posts( $args );
   
    if( have_posts() ) :
   
      // run the loop
      while( have_posts() ): the_post();
   
        // look into your theme code how the posts are inserted, but you can use your own HTML of course
        // do you remember? - my example is adapted for Twenty Seventeen theme
        //get_template_part( 'template-parts/post/content', get_post_format() );
        // for the test purposes comment the line above and uncomment the below one
        // the_title();


        get_template_part($args['template']);
   
   
      endwhile;

      echo '<div class="pagination-temp" style="display: none;">';
      get_template_part('template-parts/pagination-block');
      echo '</div>';
   
    endif;
    die; // here we exit the script and even no wp_reset_query() required!
  }
   
   
   
  add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
  add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

