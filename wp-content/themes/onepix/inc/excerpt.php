<?php
function new_excerpt_more($more) {
    global $post;
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');
