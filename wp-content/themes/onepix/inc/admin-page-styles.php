<?php

// Update CSS within in Admin
function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/libs/admin_styles/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

function remove_footer_admin (){
    $text = '<span id="footer-thankyou">Developed with love <span style="font-style: normal">❤</span></span>';
    return $text;
}
 
add_filter('admin_footer_text', 'remove_footer_admin');

