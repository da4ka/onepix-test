<?php

  function custom_time_format($minutes){

    if ( !$minutes ) return false;

    $res = '';
    $minutes = (int)$minutes;

    if ( $hours = intdiv($minutes, 60) ) $res .=  $hours . ' ч. ';

    $mins = $minutes % 60;
    $res .=  $mins . ' мин.';

    return $res;
  }